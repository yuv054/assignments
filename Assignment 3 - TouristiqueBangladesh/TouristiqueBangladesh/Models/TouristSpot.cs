﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TouristiqueBangladesh.Models
{
    public class TouristSpot
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Range(1, 5)]
        public int Rating { get; set; }

        [Display(Name = "Picture")]
        public string ImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }
    }

    public class TouristSpotDBContext : DbContext
    {
        public DbSet<TouristSpot> TouristSpots { get; set; }
    }
}