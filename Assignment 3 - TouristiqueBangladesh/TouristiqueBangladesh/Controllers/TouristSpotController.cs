﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TouristiqueBangladesh.Models;
using System.IO;
using System.Net;
using System.Data.Entity;

namespace TouristiqueBangladesh.Controllers
{
    public class TouristSpotController : Controller
    {
        private TouristSpotDBContext db = new TouristSpotDBContext();

        // GET: TouristSpot
        public ActionResult Index(string searchText, string sortBy)
        {
            var touristSpots = db.TouristSpots.AsQueryable();
            if(!String.IsNullOrEmpty(searchText))
            {
                touristSpots = touristSpots.Where(s => s.Name.ToLower().Contains(searchText.ToLower()) || s.Address.ToLower().Contains(searchText.ToLower()));
            }
            switch(sortBy)
            {
                case "Rating":
                    touristSpots = touristSpots.OrderBy(x => x.Rating);
                    break;
                case "Rating desc":
                    touristSpots = touristSpots.OrderByDescending(x => x.Rating);
                    break;
                case "Name desc":
                    touristSpots = touristSpots.OrderByDescending(x => x.Name);
                    break;
                default:
                    touristSpots = touristSpots.OrderBy(x => x.Name);
                    break;
            }
            ViewBag.SortNameParameter = String.IsNullOrEmpty(sortBy) ? "Name desc" : "";
            ViewBag.SortRatingParameter = sortBy == "Rating" ? "Rating desc" : "Rating";
            return View(touristSpots.ToList());
        }

        // GET: TouristSpot/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TouristSpot spot = db.TouristSpots.Find(id);
            if (spot == null)
            {
                return HttpNotFound();
            }
            return View(spot);
        }

        // GET: TouristSpot/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TouristSpot/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TouristSpot spot)
        {
            string path = Server.MapPath("~/App_Data/Images");
            string imageName = Path.GetFileName(spot.ImageFile.FileName);
            string fullPath = Path.Combine(path, imageName);

            spot.ImagePath = fullPath;
            spot.ImageFile.SaveAs(fullPath);


            if (ModelState.IsValid)
            {
                db.TouristSpots.Add(spot);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(spot);
        }

        // GET: TouristSpot/Edit/1
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TouristSpot spot = db.TouristSpots.Find(id);
            if(spot == null)
            {
                return HttpNotFound();
            }
            return View(spot);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, Address, Rating, ImageFile")] TouristSpot spot)
        {

            if(ModelState.IsValid)
            {
                db.Entry(spot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(spot);
        }

        // GET: TouristSpot/Delete/1
        public ActionResult Delete(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TouristSpot spot = db.TouristSpots.Find(id);
            if (spot == null)
            {
                return HttpNotFound();
            }
            return View(spot);
        }
    }
}