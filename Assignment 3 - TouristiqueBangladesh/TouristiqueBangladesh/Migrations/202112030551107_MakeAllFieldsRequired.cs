﻿namespace TouristiqueBangladesh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeAllFieldsRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TouristSpots", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.TouristSpots", "Address", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TouristSpots", "Address", c => c.String());
            AlterColumn("dbo.TouristSpots", "Name", c => c.String());
        }
    }
}
