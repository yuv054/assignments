﻿namespace TouristiqueBangladesh.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImageFileProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TouristSpots", "ImagePath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TouristSpots", "ImagePath");
        }
    }
}
