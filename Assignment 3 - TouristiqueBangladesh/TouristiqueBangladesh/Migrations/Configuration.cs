﻿namespace TouristiqueBangladesh.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TouristiqueBangladesh.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TouristiqueBangladesh.Models.TouristSpotDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TouristiqueBangladesh.Models.TouristSpotDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.TouristSpots.AddOrUpdate(i => i.Id,
                new TouristSpot
                {
                    Id = 1,
                    Name = "Sajek",
                    Address = "Sajek",
                    Rating = 4,
                    ImagePath = @"D:\Enosis\projects\Assignments\Assignment 3 - TouristiqueBangladesh\TouristiqueBangladesh\App_Data\Images\croped.jpg"
                },
                new TouristSpot
                {
                    Id = 2,
                    Name = "Nafakhum",
                    Address = "Rangamati",
                    Rating = 4,
                    ImagePath = @"D:\Enosis\projects\Assignments\Assignment 3 - TouristiqueBangladesh\TouristiqueBangladesh\App_Data\Images\croped.jpg"
                }
            );
        }
    }
}
