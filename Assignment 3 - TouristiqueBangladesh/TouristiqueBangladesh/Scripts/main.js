﻿$(document).ready(() => {
    // ==================================== Variables ==================================== //

    let touristPlaceList = '@Html.Raw(Json.Encode(Model.TouristSpot))';
    let isValidImage = true;
    let imageCode = "";
    let sortByNameState = "none";
    let sortByRatingState = "none";
    console.log("Khanki magir pola");
    console.log(@(Model));

    //// AJAX call to fetch initial data from json file 
    //$.ajax({
    //    type: "GET",
    //    url: '../data/initial_data.json',
    //    async: false,    // making it false to call the ajax before document ready
    //    success: function (json_data, status, xhr) {
    //        for (let i = 0; i < json_data["data"].length; i++) {
    //            touristPlaceList.push(json_data["data"][i]);
    //        }
    //    }
    //})

    //// ==== Function for writing all the info in the table === //
    //function fillTable() {
    //    $("tbody").empty();
    //    let i = 0;
    //    for (let place of touristPlaceList) {
    //        if (place.isShown == true) {
    //            let row = document.createElement("tr");
    //            row.setAttribute("id", `${place.id}`);
    //            row.innerHTML = `<td>${place.name}</td>
    //                            <td>${place.addr}</td>
    //                            <td>${place.rating}/5</td>
    //                            <td><img src=${place.pic}></td>
    //                            <td><button class="btn updateBtn">Update</button></td>
    //                            <td><button class="btn deleteBtn">Delete</button></td>
    //                            `;
    //            $("tbody").append(row);
    //        }
    //    }
    //}


    //// Function for Delete Row button
    //// Here I use Event Delegation as I deleted the button in fillTable() function and thus the event lost.
    //// for more "https://learn.jquery.com/events/event-delegation/"
    //$("body").on("click", ".deleteBtn", function () {
    //    let rowId = $(this).parent().parent().attr("id");
    //    rowId = touristPlaceList.findIndex(x => x["id"] == rowId);
    //    if (confirm('Are you sure you want to delete the row?')) {
    //        touristPlaceList.splice(rowId, 1);
    //        fillTable();
    //    }
    //});


    //// ================================ Click UPDATE Record Button ====================================//
    //// Update Row Button Functionalities
    //$("body").on("click", ".updateBtn", function () {
    //    let rowId = $(this).parent().parent().attr("id");
    //    let listIndex = touristPlaceList.findIndex(x => x["id"] == rowId);
    //    $(`tr#${rowId}`).empty();
    //    $(`tr#${rowId}`)
    //        .append(`<td>
    //                <div class="field">
    //                    <input type="text" name="name" id="name${rowId}" value="${touristPlaceList[listIndex].name}">
    //                    <div class="message" id="nameError${rowId}" style="display: none;">Name Can not be Empty</div>
    //                </div>
    //            </td>
    //            <td>
    //                <div class="field">
    //                <input type="text" name="address" id="address${rowId}" value="${touristPlaceList[listIndex].addr}">
    //                    <div class="message" id="addressError${rowId}" style="display: none;">Address Can not be Empty</div>
    //                </div>
    //            </td>
    //            <td>
    //                <div class="field slidecontainer">
    //                    <div class="rating">
    //                        <input type="range" min="1" max="5" value="${touristPlaceList[listIndex].rating}" step="0.5" class="slider" id="ratingSlider${rowId}">
    //                        <span id="ratingValue${rowId}">${touristPlaceList[listIndex].rating}/5</span>
    //                    </div>
    //                </div>
    //            </td>
    //            <td>
    //                <div class="field">
    //                    <input type="file" name="picture" id="picture${rowId}" accept="image/*">
    //                    <div class="message" id="imageError${rowId}" style="display: none;">Please upload a valid Image.</div>
    //                </div>                
    //            </td>
    //            <td>
    //                <button class="btn saveUpdateBtn">Save</button>
    //            </td>
    //            <td>
    //                <button class="btn cancelUpdateBtn">Cancel</button>
    //            </td>
    //            `);

    //    // Input Validation
    //    $("body").on("change blur", `#name${rowId}`, function () {
    //        if (($(`#name${rowId}`).val() == "")) $(`#nameError${rowId}`).css("display", "block");
    //        else $(`#nameError${rowId}`).css("display", "none");
    //    })

    //    $("body").on("change blur", `#address${rowId}`, function () {
    //        if (($(`#address${rowId}`).val() == "")) $(`#addressError${rowId}`).css("display", "block");
    //        else $(`#addressError${rowId}`).css("display", "none");
    //    })
    //    // Rating Slider 
    //    $(`#ratingSlider${rowId}`).change(function () {
    //        $(`#ratingValue${rowId}`)[0].innerText = $(this).val() + "/5";
    //    });
    //    // Image Validation
    //    $("body").on("change", `#picture${rowId}`, function () {
    //        let fileInput = $(this);
    //        if (fileInput.length && fileInput[0].files && fileInput[0].files.length) {
    //            let url = window.URL || window.webkitURL;
    //            let image = new Image();
    //            image.onload = function () {
    //                $(`#imageError${rowId}`).css("display", "none");
    //                isValidImage = true;
    //                imageToBase64(fileInput);
    //            };
    //            image.onerror = function () {
    //                $(`#imageError${rowId}`).css("display", "block");
    //                isValidImage = false;
    //            };
    //            image.src = url.createObjectURL(fileInput[0].files[0]);
    //        }

    //        // Finally Update File name in the input field
    //        if ($(this).val() != "")
    //            $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    //    });
    //});


    //// ================================ CANCEL Updating ====================================//
    //// Cancel Update Button Functionalities
    //$("body").on("click", ".cancelUpdateBtn", function () {
    //    let rowId = $(this).parent().parent().attr("id");
    //    let listIndex = touristPlaceList.findIndex(x => x["id"] == rowId);
    //    $(`tr#${rowId}`).empty();
    //    $(`tr#${rowId}`).append(`<td>${touristPlaceList[listIndex].name}</td>
    //                        <td>${touristPlaceList[listIndex].addr}</td>
    //                        <td>${touristPlaceList[listIndex].rating}/5</td>
    //                        <td><img src=${touristPlaceList[listIndex].pic}></td>
    //                        <td><button class="btn updateBtn">Update</button></td>
    //                        <td><button class="btn deleteBtn">Delete</button></td>
    //                        `);
    //})

    //// ================================ SAVE Update ====================================//
    //// Save Update Button Functionalities
    //$("body").on("click", ".saveUpdateBtn", function () {
    //    let rowId = $(this).parent().parent().attr("id");
    //    let listIndex = touristPlaceList.findIndex(x => x["id"] == rowId);
    //    let newPlace = {}
    //    newPlace.id = touristPlaceList[listIndex].id;
    //    newPlace.name = $(`#name${rowId}`).val();
    //    newPlace.addr = $(`#address${rowId}`).val();
    //    newPlace.rating = $(`#ratingSlider${rowId}`).val();
    //    newPlace.isShown = true;
    //    if (imageCode == "")
    //        newPlace.pic = touristPlaceList[listIndex].pic;
    //    else {
    //        newPlace.pic = imageCode;
    //        imageCode = "";
    //    }
    //    if (newPlace.name == "") {
    //        $(`#nameError${rowId}`).css("display", "block");
    //    }
    //    if (newPlace.addr == "") {
    //        $(`#addressError${rowId}`).css("display", "block");
    //    }
    //    if (newPlace.name && newPlace.addr && isValidImage) {
    //        // Check if the newly updated rows is still match with the search item
    //        if (isOnSearchBar(newPlace.name) || isOnSearchBar(newPlace.addr)) {
    //            newPlace.isShown = true;
    //        }
    //        else {
    //            newPlace.isShown = false;
    //        }
    //        touristPlaceList[listIndex] = newPlace;

    //        // Rearrange the rows if any sort state is activated
    //        if (sortByNameState == "asc") sortByName(isAscending = true);
    //        else if (sortByNameState == "desc") sortByName(isAscending = false);
    //        else if (sortByRatingState == "asc") sortByRating(isAscending = true);
    //        else if (sortByRatingState == "desc") sortByRating(isAscending = false);
    //        fillTable();
    //    }
    //})

    //// Function to clear the form values
    //function clearForm() {
    //    $("#name").val("");
    //    $("#address").val("");
    //    $("#ratingValue")[0].innerText = "3/5";
    //    $("#ratingSlider").val("3");
    //    $("#picture").val("");
    //}

    //// ================================ CREATE NEW RECORD  Starts ====================================//
    //// Add New Place from the Form value
    //$("body").on("click", ".addNewPlaceBtn", function () {
    //    let newPlace = {}
    //    newPlace.id = touristPlaceList.length;
    //    newPlace.name = $("#name").val();
    //    newPlace.addr = $("#address").val();
    //    newPlace.rating = $("#ratingSlider").val();
    //    if (isOnSearchBar(newPlace.name) || isOnSearchBar(newPlace.addr)) {
    //        newPlace.isShown = true;
    //    }
    //    else {
    //        newPlace.isShown = false;
    //    }

    //    if (newPlace.name == "") {
    //        $("#nameError").css("display", "block");
    //    }
    //    if (newPlace.addr == "") {
    //        $("#addressError").css("display", "block");
    //    }
    //    if (imageCode == "") {
    //        $("#imageError").css("display", "block");
    //    }
    //    if (newPlace.name && newPlace.addr && imageCode && isValidImage) {
    //        newPlace.pic = imageCode;
    //        touristPlaceList.push(newPlace);
    //        imageCode = "";
    //        if (sortByNameState == "asc") sortByName(isAscending = true);
    //        else if (sortByNameState == "desc") sortByName(isAscending = false);
    //        else if (sortByRatingState == "asc") sortByRating(isAscending = true);
    //        else if (sortByRatingState == "desc") sortByRating(isAscending = false);
    //        fillTable();
    //        clearForm();
    //        $("#errorMessage").css("display", "none");
    //        $("#imageError").css("display", "none");
    //    }
    //});

    //$("#name").on("change blur", function () {
    //    if (($("#name").val() == "")) $("#nameError").css("display", "block");
    //    else $("#nameError").css("display", "none");
    //})

    //$("#address").on("change blur", function () {
    //    if (($("#address").val() == "")) $("#addressError").css("display", "block");
    //    else $("#addressError").css("display", "none");
    //})
    //// Check Form Validation
    //function isValid(obj) {
    //    if (obj.name == "" || obj.addr == "" || obj.rating == "" || obj.pic == "")
    //        return false;
    //    return isValidImage;
    //}

    //// Check if valid image is give
    //$("body").on("change", '#picture', function () {
    //    let fileInput = $(this);
    //    if (fileInput.length && fileInput[0].files && fileInput[0].files.length) {
    //        let url = window.URL || window.webkitURL;
    //        let image = new Image();
    //        image.onload = function () {
    //            $("#imageError").css("display", "none");
    //            isValidImage = true;
    //            imageToBase64(fileInput);
    //        };
    //        image.onerror = function () {
    //            $("#imageError").css("display", "block");
    //            isValidImage = false;
    //        };
    //        image.src = url.createObjectURL(fileInput[0].files[0]);
    //    }

    //    // Finally Update File name in the input field
    //    if ($(this).val() != "")
    //        $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    //});

    //// Convert Uploaded Image to base64 encoding
    //function imageToBase64(fileInput) {
    //    if (fileInput.length && fileInput[0].files && fileInput[0].files.length) {
    //        let file = fileInput[0].files[0];
    //        let reader = new FileReader();
    //        reader.onload = function (e) {
    //            imageCode = e.target.result;
    //        }
    //        reader.readAsDataURL(file);
    //    }
    //}

    //// ================================ CREATE NEW RECORD  Ends ====================================//


    //// ================================ S O R T I N G  Starts ====================================//
    //function sortByName(isAscending) {
    //    if (isAscending == true) {
    //        touristPlaceList.sort((a, b) => {       // Compare Function
    //            if (a.name < b.name) return -1;
    //            if (a.name > b.name) return 1;
    //            return 0;
    //        })
    //    }
    //    else {
    //        touristPlaceList.sort((a, b) => {       // Compare Function
    //            if (a.name > b.name) return -1;
    //            if (a.name < b.name) return 1;
    //            return 0;
    //        })
    //    }
    //}

    //function sortByRating(isAscending) {
    //    if (isAscending == true) {
    //        touristPlaceList.sort((a, b) => {       // Compare Function
    //            if (a.rating < b.rating) return -1;
    //            if (a.rating > b.rating) return 1;
    //            return 0;
    //        })
    //    }
    //    else {
    //        touristPlaceList.sort((a, b) => {       // Compare Function
    //            if (a.rating > b.rating) return -1;
    //            if (a.rating < b.rating) return 1;
    //            return 0;
    //        })
    //    }
    //}

    //// Sort Rows By Name
    //$("#ascending-name").click(function () {
    //    sortByName(isAscending = true);
    //    sortByNameState = "asc";
    //    sortByRatingState = "none";
    //    fillTable();
    //    $(this).css("color", "red");
    //    $("#descending-name").css("color", "white");
    //    $("#ascending-rating").css("color", "white");
    //    $("#descending-rating").css("color", "white");
    //});

    //$("#descending-name").click(function () {
    //    sortByName(isAscending = false);
    //    sortByNameState = "desc";
    //    sortByRatingState = "none";
    //    fillTable();
    //    $(this).css("color", "red");
    //    $("#ascending-name").css("color", "white");
    //    $("#ascending-rating").css("color", "white");
    //    $("#descending-rating").css("color", "white");
    //});


    //// Sort Rows By Ratings
    //$("#ascending-rating").click(function () {
    //    sortByRating(isAscending = true);
    //    sortByRatingState = "asc";
    //    sortByNameState = "none";
    //    fillTable();
    //    $(this).css("color", "red");
    //    $("#descending-rating").css("color", "white");
    //    $("#ascending-name").css("color", "white");
    //    $("#descending-name").css("color", "white");
    //});

    //$("#descending-rating").click(function () {
    //    sortByRating(isAscending = false);
    //    sortByRatingState = "desc";
    //    sortByNameState = "none";
    //    fillTable();
    //    $(this).css("color", "red");
    //    $("#ascending-rating").css("color", "white");
    //    $("#ascending-name").css("color", "white");
    //    $("#descending-name").css("color", "white");
    //});

    //// ================================ S O R T I N G  Ends ====================================//



    //// =============================== S E A R C H I N G  Starts =================================//
    //$("#search").on("input paste", function () {
    //    for (let place of touristPlaceList) {
    //        if (isOnSearchBar(place.name) || isOnSearchBar(place.addr)) {
    //            place.isShown = true;
    //        }
    //        else {
    //            place.isShown = false;
    //        }
    //    }
    //    fillTable();
    //});

    //function isOnSearchBar(testString) {
    //    let searchBy = $("#search").val();
    //    searchBy = searchBy.toLowerCase();
    //    if (testString.toLowerCase().match(searchBy)) {
    //        return true;
    //    }
    //    return false;
    //}
    //// =============================== S E A R C H I N G  Ends =================================//


    //// ============================= Rating Slider Functionalities =============================//
    //$("#ratingSlider").change(function () {
    //    $("#ratingValue")[0].innerText = $(this).val() + "/5";
    //});


    //fillTable();
});