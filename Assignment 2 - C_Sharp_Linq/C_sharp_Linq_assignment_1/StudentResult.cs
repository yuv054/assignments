﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp_Linq_assignment_1
{

    internal class StudentResult
    {
        public int RollNum { get; set; }
        public string Subject { get; set; }
        public double Grade { get; set; }

        public StudentResult(int roll, string subject, double grade)
        {
            RollNum = roll;
            Subject = subject; 
            Grade = grade;
        }

        public static List <StudentResult> GetAllResults(string textFile)
        {
            List <StudentResult> results = new List <StudentResult>();
            if(File.Exists(textFile))
            {
                string[] lines = File.ReadAllLines(textFile);
                foreach (string line in lines)
                {
                    string[] resultInfos = line.Split(',');
                    StudentResult result = new StudentResult(Int32.Parse(resultInfos[0]), resultInfos[1], Convert.ToDouble(resultInfos[2]));
                    results.Add(result);
                }
            }
            return results;
        }

        public override string ToString()
        {
            return $"Roll: {RollNum}, Subject: {Subject}, CGPA: {Grade}";
        }
    }
}
