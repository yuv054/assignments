﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C_sharp_Linq_assignment_1
{
    internal class FinalOutput
    {
        public string StudentName { get; set; }
        public int StudentRoll { get; set; }
        public double StudentCgpa { get; set; }

        public FinalOutput(string name, int roll, double cgpa)
        {
            StudentName = name;
            StudentRoll = roll;
            StudentCgpa = cgpa;
        }
    }
}
