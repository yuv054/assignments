﻿using System;

namespace C_sharp_Linq_assignment_1
{
	public class StudentOutput
	{
		public string StudentName { get; set; }
		public int RollNumber { get; set; }
		public double CGPA { get; set; }

		public StudentOutput(string name, int roll, double cgpa)
		{
			StudentName = name;
			RollNumber = roll;	
			CGPA = cgpa;
		}
	}
}
