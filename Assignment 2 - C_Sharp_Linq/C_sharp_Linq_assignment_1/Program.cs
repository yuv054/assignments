﻿using System.Runtime.CompilerServices;
namespace C_sharp_Linq_assignment_1
{
    public class Program
    {
        private static string GetThisFilePath([CallerFilePath] string path = null)
        {
            return path;
        }

        private static void WriteAllStudentsToFile(string textFile, FinalOutput output)
        {
            if (File.Exists(textFile))
            {
                File.AppendAllText(textFile, ObjectToString(output) + Environment.NewLine);
            }
            else
            {
                File.WriteAllText(textFile, ObjectToString(output) + Environment.NewLine);
            }
        }

        private static string ObjectToString(FinalOutput obj)
        {
            return $"Name: {obj.StudentName}, Roll Number: {obj.StudentRoll}, CGPA: {obj.StudentCgpa}";
        }

        public static void Main(string[] args)
        {
            string studentFile = Path.GetDirectoryName(GetThisFilePath()) + @"\StudentInfo.txt"; ;
            string studentResultFile = Path.GetDirectoryName(GetThisFilePath()) + @"\StudentResultInfo.txt";
            string outputFile = Path.GetDirectoryName(GetThisFilePath()) + @"\Output.txt";

            List<Student> students = Student.GetAllStudents(studentFile);
            List<StudentResult> studentResults = StudentResult.GetAllResults(studentResultFile);
            List<FinalOutput> finalOutput = new List<FinalOutput>();

           // LINQ with Join
           //var joinResult = students.Join(studentResults.GroupBy(result => result.RollNum)
           //                                              .Select(g => new
           //                                              {
           //                                                  StudentRoll = g.Key,
           //                                                  AvgGrade = Math.Round(g.Average(x => x.Grade), 2)
           //                                              }),
           //                                student => student.RollNum,
           //                                groupResult => groupResult.StudentRoll,
           //                                (student, result) => new
           //                                {
           //                                    StudentName = student.Name,
           //                                    StudentRoll = student.RollNum,
           //                                    CGPA = result.AvgGrade,
           //                                })
           //                        .OrderBy(s => s.StudentRoll);

           // foreach (var result in joinResult)
           // {
           //     FinalOutput output = new FinalOutput(result.StudentName, result.StudentRoll, result.CGPA);
           //     finalOutput.Add(output);
           // }



            // LINQ with GroupJoin
            var joinResult = students.GroupJoin(studentResults.GroupBy(result => result.RollNum)
                                                              .Select(g => new
                                                              {
                                                                  StudentRoll = g.Key,
                                                                  AvgGrade = Math.Round(g.Average(x => x.Grade), 2)
                                                              }),
                                                student => student.RollNum,
                                                result => result.StudentRoll,
                                                (student, result) => new FinalOutput
                                                {
                                                    StudentName = student.Name,
                                                    StudentRoll = student.RollNum,
                                                    Result = result
                                                })
                                      .OrderBy(student => student.StudentRoll)
                                      .ToList();


            foreach (var group in joinResult)
            {
                try
                {
                    FinalOutput output = new FinalOutput(group.StudentName, group.StudentRoll, group.Result.First().AvgGrade);
                    finalOutput.Add(output);
                }
                catch { }
            }

            foreach (var output in finalOutput)
            {
                WriteAllStudentsToFile(outputFile, output);
            }

        }
    }
}