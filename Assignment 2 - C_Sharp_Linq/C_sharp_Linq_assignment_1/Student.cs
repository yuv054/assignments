﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace C_sharp_Linq_assignment_1
{
    internal class Student
    {
        public string Name { get; set; }
        public int RollNum { get; set; }
        public string Address { get; set; }

        public Student(string name, int roll, string address)
        {
            Name = name;
            RollNum = roll;
            Address = address;
        }

        public static List<Student> GetAllStudents(string textFile)
        {
            List<Student> students = new List<Student>();
            if (File.Exists(textFile))
            {
                string[] lines = File.ReadAllLines(textFile);
                foreach (var line in lines)
                {
                    string[] infos = line.Split(',');
                    Student std = new Student(infos[0], Int32.Parse(infos[1]), infos[2]);
                    students.Add(std);
                    //Console.WriteLine(std);
                }
            }
            return students;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Roll Number: {RollNum}";
        }

    }
}
